<div class="sidebar">
      <h3 class="title--big">Hot this month</h3>
      <!-- MỖI THẺ A LÀ 1 QUẢNG CÁO  -->
      <a class="codepen-item pie" href="" target="_blank"><img class="pie__image" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/567707/showcase-pie.jpg">
        <div class="pie__subtitle">Food </div>
        <div class="pie__content">
          <h4>Pies for everyone!</h4>
          <p>No man is left behind. There's infinite pie with this one div and a repeating background.</p>
        </div>
      </a>
      <a class="sidebar-item captcha" href="" target="_blank">
        <h5>Frustrated designer runs amok with Captcha ideas</h5>
        <p>From Tic Tac Toe to solving meme-based questions, this is a different take on how web captchas should be. "I have not successfully picked out all photos of a truck on the first try. Something's gotta change," says the designer, who has requested to remain anonymous.</p>
      </a>
      <a class="sidebar-item slack-ui with-border" href="" target="_blank">
        <h5>Slack UI gets reverse engineered</h5>
        <p>Another valiant effort to reverse engineer a web app. However, the UI is repurposed to showcase Codepens instead of mock conversations. This is a codepen showcase inception situation.</p>
      </a>
      <a class="workout" href="" target="_blank">
        <div class="workout__image"><img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/567707/showcase-workout.jpg" alt="Workout"></div>
        <div class="workout__blurb">Always failing to keep track of your workouts? </div>
        <div class="workout__title">Use this tool!</div>
      </a>
    </div>